//
//  ViewController.swift
//  Flyadeal
//
//  Created by Md.Ballal Hossen on 6/12/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate  {
    
    
    @IBOutlet weak var homeMenuCollectionView: UICollectionView!
    
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var sliderWebView: UIWebView!
    
    @IBOutlet weak var logoForAr: UIImageView!
    
    @IBOutlet weak var logoForEn: UIImageView!
    
    @IBOutlet weak var pagerScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var bookNowButton: UIButton!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    
    @IBOutlet weak var welcome2Label: UILabel!
    
    @IBOutlet weak var backGroundImageView: UIImageView!
    
    
    var slideImages: [UIImage] = []
    var menuArray = [Dictionary<String,Any>]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sliderWebView.backgroundColor = UIColor.clear
        sliderWebView.isOpaque = false
        
        self.bookNowButton.setTitle("Book now".localized, for: .normal)
        self.welcomeLabel.text = "Welcome".localized
        self.welcome2Label.text = "Welcome".localized
        
        
        welcomeLabel.roundCorners([.topLeft,.topRight], radius: 5)
        
        //        self.backGroundImageView.image = UIImage(named:"background".localized)

        homeMenuCollectionView.delegate = self
        homeMenuCollectionView.dataSource = self
        
        menuArray.append(["title":"Book flights".localized, "icon":"flight_icon"])
        menuArray.append(["title":"Manage".localized, "icon":"manage"])
        menuArray.append(["title":"Check-in".localized, "icon":"check-in"])
        menuArray.append(["title":"flyadeal.com".localized, "icon":"passes"])

//        if Language.language == Language.english{
//
//           slideImages += [UIImage(named: "Slide 1.png")!, UIImage(named: "Slide 2.png")!,UIImage(named: "Slide 3.png")!,UIImage(named: "Slide 4.png")!,UIImage(named: "Slide 5.png")!,UIImage(named: "Slide 6.png")!]
//        }else{
//
//            slideImages += [UIImage(named: "Slide 1-ar.png")!, UIImage(named: "Slide 2-ar.png")!,UIImage(named: "Slide 3-ar.png")!,UIImage(named: "Slide 4-ar.png")!,UIImage(named: "Slide 5-ar.png")!,UIImage(named: "Slide 6-ar.png")!]
//        }
        
        
        //setPager()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        var sliderUrl = "http://flyadeal.com/en/sliderapp"
        
        if Language.language == Language.english{
          
            print("??????????",Language.language)
            welcomeLabel.isHidden = false
            welcome2Label.isHidden = true
            logoForEn.isHidden = false
            logoForAr.isHidden = true
            
            bookNowButton.titleLabel?.font = UIFont(name:"Raleway-Bold",size:20)

            
        }else{
            
            
            sliderUrl = "http://flyadeal.com/ar/sliderapp"
            
            logoForEn.isHidden = true
            logoForAr.isHidden = false
            

            welcomeLabel.isHidden = true
            welcome2Label.isHidden = false
            
           bookNowButton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:20)
            
        }
        
        
        if let url = URL(string: sliderUrl) {
            print("Slider Url",url)
            let request = URLRequest(url: url)
            sliderWebView.loadRequest(request)
        }


    }
    
//    func setPager() {
//
//
//        self.pagerScrollView.frame = CGRect(x:20, y:self.pagerScrollView.frame.origin.y, width:self.view.frame.width - 40, height:(self.view.frame.size.height * (120/667)))
//
//        let scrollViewWidth:CGFloat = self.pagerScrollView.frame.width
//        let scrollViewHeight:CGFloat = self.pagerScrollView.frame.height + CGFloat(20)
//
//        var xPosition : CGFloat = 0
//
//        for i in 0..<slideImages.count {
//
//
//            let imageView = UIImageView(frame: CGRect(x:xPosition, y:0,width:scrollViewWidth, height:scrollViewHeight))
//            imageView.contentMode = .scaleAspectFit
//
//
//            imageView.image = slideImages[i]
//
//            self.pagerScrollView.addSubview(imageView)
//
//            xPosition = scrollViewWidth * CGFloat(i + 1)
//
//            print("xPosition && scrollViewWidth",imageView.frame,scrollViewWidth)
//        }
//
//
//
//        let count = slideImages.count
//
//        self.pagerScrollView.contentSize = CGSize(width:self.pagerScrollView.frame.width * CGFloat(count), height:self.pagerScrollView.frame.height)
//        self.pagerScrollView.delegate = self
//        self.pageControl.currentPage = 0
//
//
//
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped(tapGestureRecognizer:)))
//        tapGestureRecognizer.cancelsTouchesInView = false
//        pagerScrollView.addGestureRecognizer(tapGestureRecognizer)
//
//
//
//        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
//
//
//    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // your layout code goes here, AFTER the call to super
    }
    
    @objc func tapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        print("tap ...............")
    }
    
    @objc func moveToNextPage (){
        
        let pageWidth:CGFloat = self.pagerScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 6
        let contentOffset:CGFloat = self.pagerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
//        print("pagerScrollView.frame.width",pagerScrollView.frame.width)
//        print("contentOffset :\(contentOffset),slideToX :\(slideToX)  maxWidth : \(maxWidth)")
//
        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
        }
        self.pagerScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.pagerScrollView.frame.height), animated: true)
    }
    
    


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return menuArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:UICollectionViewCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "homeMenuCell", for: indexPath )
        
        print("cell.frame.size.height",cell.frame.size.height)
        
        let imageView = cell.viewWithTag(1) as! UIImageView
       
        imageView.image = UIImage(named: menuArray[indexPath.item]["icon"]! as! String)
        
        let title = cell.viewWithTag(2) as! UILabel
        
        
        if Language.language == Language.english{
            
            title.font = UIFont(name:"Raleway",size:18)
            
        }else{
            
            title.font = UIFont(name:"Cairo",size:18)
        }
        
        
        
        if cell.frame.size.height < 95{
        
            title.font = title.font.withSize(15)

        }
        title.text = menuArray[indexPath.item]["title"]! as? String
        
       
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if indexPath.row == 0{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookFlightViewController") as! BookFlightViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManageViewController") as! ManageViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 2{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 3{
            
            let urlString = "https://www.flyadeal.com"
            
            if let url = URL(string:urlString ) {
               print("indexPath.row == 3")
               UIApplication.shared.open(url, options: [:])
            }
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let padding: CGFloat =  10
        
        
        return CGSize(width: (collectionView.frame.size.width - padding
            )/2, height: (collectionView.frame.size.height - padding)/2)
    }
    
    
    @IBAction func changeLanguageButtonAction(_ sender: Any) {
        
        
        if Language.language == Language.english{
            
            
            Language.language = Language.arabic
            print("??????????",Language.language)
            
        }else{
            
            Language.language = Language.english
           
            
        }
    }
    
    
    @IBAction func bookFlightButtonAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookFlightViewController") as! BookFlightViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}

private typealias ScrollView = HomeViewController
extension ScrollView
{
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/3)/pageWidth)+1
        // Change the indicator
        
        //print("currentPage",currentPage)
        
        self.pageControl.currentPage = Int(currentPage);
       
    }
}


