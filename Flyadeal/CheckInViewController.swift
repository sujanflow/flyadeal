//
//  CheckInViewController.swift
//  Flyadeal
//
//  Created by Md.Ballal Hossen on 11/12/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import SafariServices

class CheckInViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var welComeENLabel: UILabel!
    @IBOutlet weak var welcomeARLabel: UILabel!
    
    @IBOutlet weak var logoForAr: UIImageView!
    
    @IBOutlet weak var logoForEn: UIImageView!
    
    @IBOutlet weak var checkInLabel: UILabel!
    
    @IBOutlet weak var bookingRefView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    
    @IBOutlet weak var bookingRefTopLabel: UILabel!
    
   
    @IBOutlet weak var lastNtopLabel: UILabel!
    
    
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    
    
    
    @IBOutlet weak var popUpView: UIView!

    @IBOutlet weak var bookRefPopView: UIView!
    @IBOutlet weak var lastNamePopView: UIView!
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var lastNameTopLabel: UILabel!
    
    
    @IBOutlet weak var referenceTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var okRefButton: UIButton!
    @IBOutlet weak var okLNamebutton: UIButton!
    
    @IBOutlet weak var refCancelbutton: UIButton!
    
    @IBOutlet weak var lNameCancleButton: UIButton!
    
    
    
    @IBOutlet weak var continueButton: UIButton!
    
    
    var isEditingBookingRef:Bool = false
    var isEditingLastName:Bool = false
    
    var culture:String = "en-GB"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.welComeENLabel.text = "Welcome".localized
        self.welcomeARLabel.text = "Welcome".localized
        checkInLabel.text = "Check In".localized
        bookingRefTopLabel.text = "Booking Reference".localized
        lastNtopLabel.text = "Last Name".localized
        
        continueButton.setTitle("Continue".localized, for: .normal)
        topLabel.text = "Booking Reference".localized
        lastNameTopLabel.text = "Last Name".localized
        
        
        okRefButton.setTitle("OK".localized, for: .normal)
        refCancelbutton.setTitle("Cancel".localized, for: .normal)
        okLNamebutton.setTitle("OK".localized, for: .normal)
        lNameCancleButton.setTitle("Cancel".localized, for: .normal)


//        topLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        lastNameTopLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        
        
        let tapOnReferenceView = UITapGestureRecognizer(target: self, action: #selector(tappedInFeferenceView(tapGestureRecognizer:)))
        
        bookingRefView.addGestureRecognizer(tapOnReferenceView)
        
        let tapOnNameView = UITapGestureRecognizer(target: self, action: #selector(tappedInNameView(tapGestureRecognizer:)))
        
        lastNameView.addGestureRecognizer(tapOnNameView)

        
        referenceTextField.delegate = self
        lastNameTextField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Language.language == Language.english{
            
            print("??????????",Language.language)
            
            welComeENLabel.isHidden = false
            welcomeARLabel.isHidden = true
            
            logoForEn.isHidden = false
            logoForAr.isHidden = true
            

            culture = "en-GB"
            
        }else{
            
            logoForEn.isHidden = true
            logoForAr.isHidden = false
            

            welComeENLabel.isHidden = true
            welcomeARLabel.isHidden = false
            
            culture = "ar-SA"
            
            
            checkInLabel.font = UIFont(name:"Cairo-Bold",size:20)
            
            bookingRefTopLabel.font = UIFont(name:"Cairo",size:13)
            lastNtopLabel.font = UIFont(name:"Cairo",size:13)
            referenceLabel.font = UIFont(name:"Cairo",size:18)
            lastNameLabel.font = UIFont(name:"Cairo",size:18)
            
            topLabel.font = UIFont(name:"Cairo-Bold",size:18)
            
            lastNameTopLabel.font = UIFont(name:"Cairo-Bold",size:18)
            
            
            
            
            continueButton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            okRefButton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            refCancelbutton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            okLNamebutton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            
            lNameCancleButton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            
            
        }
        
    }
    
    @objc func tappedInFeferenceView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("tap .............tappedInFeferenceView..")
        
        bookRefPopView.isHidden = false
        lastNamePopView.isHidden = true
        
        showPopUpView()
        
        self.referenceTextField.becomeFirstResponder()
    }
    
    @objc func tappedInNameView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("tap .............tappedInNameView..")
        
        bookRefPopView.isHidden = true
        lastNamePopView.isHidden = false
        
        showPopUpView()
        
        self.lastNameTextField.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        referenceTextField.resignFirstResponder()
        lastNameTextField.resignFirstResponder()
    }
    
    
    func showPopUpView(){
        
        popUpView.isHidden = false
        
        popUpView.frame = CGRect(x:0, y:self.view.frame.height, width:self.popUpView.frame.width, height:self.popUpView.frame.size.height)
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.popUpView.frame = CGRect(x:0, y:0, width:self.popUpView.frame.width, height:self.popUpView.frame.size.height)
            
            
        }) { (true) in
            
         
            
            print("animate done")
            
        }
    }
    
    func hidePopUpView(){
        
        //        self.popUpView.frame = CGRect(x:0, y:0, width:self.popUpView.frame.width, height:self.popUpView.frame.size.height)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.popUpView.frame = CGRect(x:0, y:self.view.frame.height, width:self.popUpView.frame.width, height:self.popUpView.frame.size.height)
            
            
        }) { (true) in
            
            print("animate hide")
            self.referenceTextField.resignFirstResponder()
            self.lastNameTextField.resignFirstResponder()
            
            self.popUpView.isHidden = true
            
            if self.isEditingBookingRef{
                
                self.referenceLabel.text = self.referenceTextField.text
                self.isEditingBookingRef = false
                self.referenceTextField.resignFirstResponder()
                
            }
            if self.isEditingLastName{
                
                self.lastNameLabel.text = self.lastNameTextField.text
                self.isEditingLastName = false
                self.lastNameTextField.resignFirstResponder()
                
            }
            
        }
        
    }
    
    @IBAction func bookingRefOkButtonAction(_ sender: Any) {
        
        if !((referenceTextField.text?.isEmpty)!){
            
            
            if !((referenceTextField.text?.count)! == 6){
                
                print("referenceLabel.text?.count",(referenceTextField.text?.count)!)
                
                let alertVC = UIAlertController(title: nil, message: "Please ensure that Booking Reference is 6 characters.".localized, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                    self.dismiss(animated: true, completion: nil)
                    
                })
                
                alertVC.addAction(okAction)
                self.present(alertVC, animated: true, completion: nil)
                
            }else{
               isEditingBookingRef = true
            
               hidePopUpView()
            
            }
            
        }else{
            
            let alertVC = UIAlertController(title: nil, message: "Please enter booking reference.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alertVC.addAction(okAction)
            self.present(alertVC, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func lastNameOkButtonAction(_ sender: Any) {
        
        if !((lastNameTextField.text?.isEmpty)!){
            
            isEditingLastName = true
            
            hidePopUpView()
            
            
            
        }else{
            
            let alertVC = UIAlertController(title: nil, message: "Please enter your last name.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alertVC.addAction(okAction)
            self.present(alertVC, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        hidePopUpView()
    }
    
    
    @IBAction func languageButtonAction(_ sender: Any) {
        
        if Language.language == Language.english{
            
            
            Language.language = Language.arabic
            print("??????????",Language.language)
            
        }else{
            
            Language.language = Language.english
            
            
        }
    }
    
    
    @IBAction func continueButtonAction(_ sender: Any) {
        
        if !((referenceLabel.text?.isEmpty)!) &&  !((lastNameLabel.text?.isEmpty)!){
            
//            if !((referenceLabel.text?.count)! == 6){
//
//                let alertVC = UIAlertController(title: nil, message: "Please ensure that Booking Reference is 6 characters.".localized, preferredStyle: .alert)
//                let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
//                    self.dismiss(animated: true, completion: nil)
//
//                })
//
//                alertVC.addAction(okAction)
//                self.present(alertVC, animated: true, completion: nil)
//
//            }else{
            
                let urlString = "https://book.flyadeal.com/#/checkin/search-redirect?culture=\(culture)&pnr=\(referenceLabel.text!)&lastName=\(lastNameLabel.text!)"
                
                print("urlString",urlString)
                
                if let url = URL(string: urlString) {
                    
                   // UIApplication.shared.open(url, options: [:])
                    let svc = SFSafariViewController(url: url)
                    
                    self.present(svc, animated: true) {
                        
                        print("open safari")
                    }

                    
                }
          //  }
        }else{
            
            let alertVC = UIAlertController(title: nil, message: "Please give your input correctly.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alertVC.addAction(okAction)
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func homeButtonAction(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
}
