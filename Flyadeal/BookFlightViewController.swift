//
//  BookFlightViewController.swift
//  Flyadeal
//
//  Created by Md.Ballal Hossen on 9/12/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import SafariServices

class BookFlightViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var welcomeLabel1: UILabel!
    @IBOutlet weak var welcomeLabel2: UILabel!
    
    @IBOutlet weak var logoForAr: UIImageView!
    
    @IBOutlet weak var logoForEn: UIImageView!
    
    @IBOutlet weak var returnButton: UIButton!
    
    @IBOutlet weak var returnLabel: UILabel!
    @IBOutlet weak var oneWayButton: UIButton!
    @IBOutlet weak var oneWayLabel: UILabel!
    
    
    @IBOutlet weak var passengersView: UIView!
    @IBOutlet weak var fromView: UIView!
    
    @IBOutlet weak var toView: UIView!
    
    @IBOutlet weak var dateView: UIView!
    
   
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var topLabel: UILabel!
    
    @IBOutlet weak var promoTopLabel: UILabel!
    
    @IBOutlet weak var passengerTopLabel: UILabel!
    
    @IBOutlet weak var selectOriginLabel: UILabel!
    
    
    
    
    @IBOutlet weak var promoTextView: UITextField!
    
    
    @IBOutlet weak var popDateView: UIView!
    
    @IBOutlet weak var popPlaceView: UIView!
    
    @IBOutlet weak var popPassengerView: UIView!
    
    @IBOutlet weak var popPromoView: UIView!
    
    
    @IBOutlet weak var closeButtonInPlaceView: UIButton!
    
    @IBOutlet weak var okButtonInPromoView: UIButton!
    
    @IBOutlet weak var cancelButtonInPromoView: UIButton!
    
    
    @IBOutlet weak var adultLabel: UILabel!
    @IBOutlet weak var childLabel: UILabel!
    @IBOutlet weak var infantLabel: UILabel!
    
    
    @IBOutlet weak var adultCountLabel: UILabel!
    @IBOutlet weak var infantCountLabel: UILabel!
    @IBOutlet weak var childCountLabel: UILabel!
    
    @IBOutlet weak var closeButtonInInfantView: UIButton!
    
    
    @IBOutlet weak var placesTableView: UITableView!
    
    //FromView
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var fromPlaceNameLabel: UILabel!
    @IBOutlet weak var fromPlaceShortLabel: UILabel!
    
    //ToView
    
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var toPlacenameLabel: UILabel!
    @IBOutlet weak var toPlaceShortLabel: UILabel!
    
    //DateView
    
    @IBOutlet weak var travelDatelabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    //PassengerView
    
    @IBOutlet weak var passengerLabel: UILabel!
    @IBOutlet weak var passengerListLabel: UILabel!
    
    
    @IBOutlet weak var searchFlightButton: UIButton!
    
    @IBOutlet weak var addPromoButton: UIButton!
    
    //********calendar view***********
    
    @IBOutlet weak var mainCalendarView: UIView!
    @IBOutlet weak var okButtonInCalendarView: UIButton!
    
    @IBOutlet weak var cancelButtonInCalendarView: UIButton!
    
    
    @IBOutlet weak var monthHeaderView: VAMonthHeaderView!{
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "LLLL"
            dateFormatter.locale = Locale.init(identifier: Locale.preferredLanguages[0])
            
            let pre = Locale.preferredLanguages[0]
            
            print("prepreprepre",pre)

            let appereance = VAMonthHeaderViewAppearance(
                previousButtonImage: #imageLiteral(resourceName: "previous"),
                nextButtonImage: #imageLiteral(resourceName: "next"),
                dateFormatter: dateFormatter
            )
            monthHeaderView.delegate = self
            monthHeaderView.appearance = appereance
            
            
        }
    }
    
    @IBOutlet weak var weekdayView: VAWeekDaysView!{
        
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .short, calendar: defaultCalendar)
            weekdayView.appearance = appereance
        }
    }
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone(secondsFromGMT: 3)!
        return calendar
    }()

    
    var calendarView: VACalendarView!
    var calendar :VACalendar? = nil
    
    //*************
    
    
  
    
    var places = [String]()
    var destinationPlaces = [String]()
    
    
    var totalPassenger = 1
    var adultPassenger = 1
    var childPassenger = 0
    var infantPassenger = 0
    
    
    var firstselectedDay = Date()
    var secendselectedDay = Date.tomorrow

    var firstdatestring:String?
    var lastdatestring:String?
    var oneWaydatestring:String?
    
    var firstReadabledatestring:String?
    var lastReadabledatestring:String?
    var oneWayReadabledatestring:String?

    
    var culture:String = "en-GB"
    
    
    var fromShortName = ""
    var toShortName = ""
    
    var isForOrigin : Bool = false
    var isForDestination : Bool = false
    var isSelectPlace : Bool = false
    
    
    var selectedPlace = ""
    
    var isPromoAdded = false
    var promoCode = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.welcomeLabel1.text = "Welcome".localized
        self.welcomeLabel2.text = "Welcome".localized
        returnLabel.text = "Return".localized
        oneWayLabel.text = "One Way".localized
        fromLabel.text = "From".localized
        toLabel.text = "To".localized
        travelDatelabel.text = "Travel Dates".localized
        passengerLabel.text = "Passengers".localized

        topLabel.text = "Select Travel Dates".localized
        selectOriginLabel.text = "Select Origin".localized
        promoTopLabel.text = "Promo Code".localized

        passengerTopLabel.text = "Please select passengers".localized
        childLabel.text = "Child".localized
        adultLabel.text = "Adult".localized
        infantLabel.text = "Infant".localized

        okButtonInCalendarView.setTitle("OK".localized, for: .normal)
        cancelButtonInCalendarView.setTitle("Cancel".localized, for: .normal)

        closeButtonInPlaceView.setTitle("Close".localized, for: .normal)
        okButtonInPromoView.setTitle("OK".localized, for: .normal)
        cancelButtonInPromoView.setTitle("Cancel".localized, for: .normal)
        closeButtonInInfantView.setTitle("Close".localized, for: .normal)


        addPromoButton.setTitle("Add Promo Code".localized, for: .normal)
        searchFlightButton.setTitle("Search Flights".localized, for: .normal)

        
        
//        print("language",Language.language)
        
        places = ["Abha".localized, "Dammam".localized, "Qassim".localized,"Jazan".localized, "Jeddah".localized, "Madinah".localized,"Riyadh".localized, "Tabuk".localized]
        
       
        
        
        
        
        
        
        placesTableView.delegate = self
        placesTableView.dataSource = self
        placesTableView.tableFooterView = UIView()
        
//        topLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        selectOriginLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        passengerTopLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        promoTopLabel.roundCorners([.topLeft,.topRight], radius: 5)
       
        
        firstdatestring = firstselectedDay.toDateString(format: "yyyy-MM-dd")
        lastdatestring = secendselectedDay.toDateString(format: "yyyy-MM-dd")
        oneWaydatestring = firstselectedDay.toDateString(format: "yyyy-MM-dd")
        
        firstReadabledatestring = firstselectedDay.toDateString(format: "d MMM, yyyy").localized
        lastReadabledatestring = secendselectedDay.toDateString(format: "d MMM, yyyy").localized
        oneWayReadabledatestring = firstselectedDay.toDateString(format: "d MMM, yyyy").localized
        
        print("oneWaydatestring",oneWaydatestring!)
       
        
        promoTextView.delegate = self
        
        setFirstView()
        
        setTapInView()
        
        //setup Celendar
        
        setCelendar()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Language.language == Language.english{
            
            print("??????????",Language.language)
            welcomeLabel1.isHidden = false
            welcomeLabel2.isHidden = true
            
            logoForEn.isHidden = false
            logoForAr.isHidden = true
            

            
            
            
            culture = "en-GB"
            
        }else{
            
            logoForEn.isHidden = true
            logoForAr.isHidden = false
            

            welcomeLabel1.isHidden = true
            welcomeLabel2.isHidden = false
            
            culture = "ar-SA"
            
            returnLabel.font = UIFont(name:"Cairo-Bold",size:15)
            oneWayLabel.font = UIFont(name:"Cairo",size:15)

            fromLabel.font = UIFont(name:"Cairo",size:13)
            toLabel.font = UIFont(name:"Cairo",size:13)
            travelDatelabel.font = UIFont(name:"Cairo",size:13)
            passengerLabel.font = UIFont(name:"Cairo",size:13)
            fromPlaceNameLabel.font = UIFont(name:"Cairo",size:18)
            toPlacenameLabel.font = UIFont(name:"Cairo",size:18)
            
            dateLabel.font = UIFont(name:"Cairo",size:18)
            passengerListLabel.font = UIFont(name:"Cairo",size:18)

            topLabel.font = UIFont(name:"Cairo-Bold",size:18)
            selectOriginLabel.font = UIFont(name:"Cairo-Bold",size:18)
            promoTopLabel.font = UIFont(name:"Cairo-Bold",size:18)
            passengerTopLabel.font = UIFont(name:"Cairo-Bold",size:18)

            childLabel.font = UIFont(name:"Cairo",size:17)
            adultLabel.font = UIFont(name:"Cairo",size:17)
            infantLabel.font = UIFont(name:"Cairo",size:17)

            okButtonInCalendarView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            cancelButtonInCalendarView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)

            closeButtonInPlaceView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            okButtonInPromoView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            cancelButtonInPromoView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            closeButtonInInfantView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)


            addPromoButton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:15)
            searchFlightButton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)

        }
        
        print("to label font",toLabel.font)
        
    }
    
    func setFirstView(){
        
//        fromPlaceNameLabel.text = "Dammam"
//        toPlacenameLabel.text = "Jeddah"
        
        fromPlaceNameLabel.text = "Dammam".localized
        toPlacenameLabel.text = "Jeddah".localized
        

        let fromShort = getShortFormForPlace(forString: fromPlaceNameLabel.text!)
        let toShort = getShortFormForPlace(forString: toPlacenameLabel.text!)
        
        fromPlaceShortLabel.text = fromShort
        toPlaceShortLabel.text = toShort
        
        print("short",getShortFormForPlace(forString: fromPlaceNameLabel.text!))
        print("toPlaceShortLabel.text......",toPlaceShortLabel.text!)
        
        
        if self.returnButton.isSelected{
            
            dateLabel.text = "\(firstReadabledatestring!) - \(lastReadabledatestring!)"
        }else{
            
            dateLabel.text = "\(oneWayReadabledatestring!)"
        }
        
        
        setPassenger()
        
    }
    
    
    func setPassenger() {
        
        let adultText = "Adult"
        let adultsText = "Adults"
        let childText = "Child"
        let childrenText = "Children"
        let infantText = "Infant"
        let infantsText = "Infants"
        
        if (childPassenger == 0 && infantPassenger == 0 && adultPassenger == 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultText.localized)"
            
        }else if(childPassenger == 0 && infantPassenger == 0 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized)"
            
        }else if(childPassenger == 1 && infantPassenger == 0 && adultPassenger == 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultText.localized) \(childPassenger) \(childText.localized) "
            
        }else if(childPassenger > 1 && infantPassenger == 0 && adultPassenger == 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultText.localized) \(childPassenger) \(childrenText.localized)"
        }else if(childPassenger == 1 && infantPassenger == 1 && adultPassenger == 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultText.localized) \(childPassenger) \(childText.localized) \(infantPassenger) \(infantText.localized)"
            
        }else if(childPassenger == 1 && infantPassenger == 0 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(childPassenger) \(childText.localized)"
            
        }else if(childPassenger > 1 && infantPassenger == 0 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(childPassenger) \(childrenText.localized) "
            
        }else if(childPassenger > 1 && infantPassenger > 1 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(childPassenger) \(childrenText.localized) \(infantPassenger) \(infantsText.localized)"
            
        }else if(childPassenger == 1 && infantPassenger > 1 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(childPassenger) \(childText.localized) \(infantPassenger) \(infantsText.localized)"
            
        }else if(childPassenger == 0 && infantPassenger > 1 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(infantPassenger) \(infantsText.localized)"
            
        }else if(childPassenger == 0 && infantPassenger == 1 && adultPassenger == 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultText.localized) \(infantPassenger) \(infantText.localized)"
        }else if(childPassenger > 1 && infantPassenger > 1 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(childPassenger) \(childrenText.localized) \(infantPassenger) \(infantsText.localized)"
            
        }else if(childPassenger > 1 && infantPassenger == 1 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(childPassenger) \(childrenText.localized) \(infantPassenger) \(infantText.localized)"
            
        }else if(childPassenger == 1 && infantPassenger == 1 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(childPassenger) \(childText.localized) \(infantPassenger) \(infantText.localized)"
            
        }else if(childPassenger == 0 && infantPassenger == 1 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(infantPassenger) \(infantText.localized)"
            
        }else if(childPassenger > 1 && infantPassenger == 1 && adultPassenger > 1){
            
            passengerListLabel.text = "\(adultPassenger) \(adultsText.localized) \(childPassenger) \(childrenText.localized) \(infantPassenger) \(infantText.localized)"
            
        }

        
    }
    
    func setTapInView(){
        
        let tapOnFromView = UITapGestureRecognizer(target: self, action: #selector(tappedInFromView(tapGestureRecognizer:)))
        
        fromView.addGestureRecognizer(tapOnFromView)
        
        let tapOnToView = UITapGestureRecognizer(target: self, action: #selector(tappedInToView(tapGestureRecognizer:)))
        
        toView.addGestureRecognizer(tapOnToView)
        
        let tapOnDateView = UITapGestureRecognizer(target: self, action: #selector(tappedInDateView(tapGestureRecognizer:)))
        
        dateView.addGestureRecognizer(tapOnDateView)
        
        let tapOnPassengerView = UITapGestureRecognizer(target: self, action: #selector(tappedInPassengerView(tapGestureRecognizer:)))
        
        passengersView.addGestureRecognizer(tapOnPassengerView)

        
    }

    @objc func tappedInFromView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("tap .............fromview..")
        isForOrigin = true
        isForDestination = false
        placesTableView.reloadData()
        
        self.selectOriginLabel.text = "Select Origin".localized
        popDateView.isHidden = true
        popPlaceView.isHidden = false
        popPassengerView.isHidden = true
        popPromoView.isHidden = true
        
        showPopUpView()
        
    }
    @objc func tappedInToView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if fromPlaceShortLabel.text == "DMM"{
            destinationPlaces = ["Abha","Jazan","Jeddah", "Madinah","Riyadh"]
            
        }else if (fromPlaceShortLabel.text == "AHB" || (fromPlaceShortLabel.text == "GIZ")){
            
            destinationPlaces = ["Dammam","Jeddah","Riyadh"]
            
        }else if (fromPlaceShortLabel.text == "JED"){
            
            destinationPlaces = ["Abha","Jazan","Dammam","Qassim","Riyadh","Tabuk"]
            
        }else if (fromPlaceShortLabel.text == "MED"){
            
            destinationPlaces = ["Dammam","Riyadh"]
        }else if (fromPlaceShortLabel.text == "ELQ"){
            
            destinationPlaces = ["Jeddah"]
        }else if (fromPlaceShortLabel.text == "RUH"){
            
            destinationPlaces = ["Abha","Jazan","Dammam","Jeddah","Madinah","Tabuk"]
        }else if (fromPlaceShortLabel.text == "TUU"){
            
            destinationPlaces = ["Jeddah","Riyadh"]
        }
        
        
        isForOrigin = false
        isForDestination = true
        placesTableView.reloadData()
        
        self.selectOriginLabel.text = "Select Destination".localized
        popDateView.isHidden = true
        popPlaceView.isHidden = false
        popPassengerView.isHidden = true
        popPromoView.isHidden = true
        
        showPopUpView()
        
        print("tap .............to..")
    }
    @objc func tappedInDateView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        print("tap .............date..")
        
        popPlaceView.isHidden = true
        popDateView.isHidden = false
        popPassengerView.isHidden = true
        popPromoView.isHidden = true
        
        showPopUpView()
    }
    @objc func tappedInPassengerView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        adultCountLabel.text = "\(adultPassenger)"
        childCountLabel.text = "\(childPassenger)"
        infantCountLabel.text = "\(infantPassenger)"

        
        popPlaceView.isHidden = true
        popDateView.isHidden = true
        popPassengerView.isHidden = false
        
        showPopUpView()
        print("tap .............passenger..")
    }
    
    func setCelendar(){
        
       let today = VADay(date: firstselectedDay , state: .selected, calendar: Calendar.current)
        let tommorow = VADay(date: secendselectedDay , state: .selected, calendar: Calendar.current)
        
        calendar = VACalendar(calendar: defaultCalendar)
        calendarView = VACalendarView(frame: .zero, calendar: calendar!)
        calendarView.showDaysOut = true
        
        
        print("calendar?.selectedDays",calendar?.selectedDays as Any)
        
        
        if self.returnButton.isSelected{
            
            calendar?.selectDay(today)
            calendar?.selectDay(tommorow)
            
            calendar?.selectedDays = [today,tommorow]

            calendarView.selectionStyle = .multi
            
        }else{
            
           
            calendar?.selectDay(today)
            
            
            calendarView.selectionStyle = .single
            
        }
        
        calendarView.monthDelegate = monthHeaderView
        calendarView.dayViewAppearanceDelegate = self
        calendarView.monthViewAppearanceDelegate = self as? VAMonthViewAppearanceDelegate
        calendarView.calendarDelegate = self
        calendarView.scrollDirection = .horizontal
        
//        calendarView.setSupplementaries([
//            (Date().addingTimeInterval(-(60 * 60 * 70)), [VADaySupplementary.bottomDots([.red, .magenta])]),
//            (Date().addingTimeInterval((60 * 60 * 110)), [VADaySupplementary.bottomDots([.red])]),
//            (Date().addingTimeInterval((60 * 60 * 370)), [VADaySupplementary.bottomDots([.blue, .darkGray])]),
//            (Date().addingTimeInterval((60 * 60 * 430)), [VADaySupplementary.bottomDots([.orange, .purple, .cyan])])
//            ])
        self.mainCalendarView.addSubview(calendarView)
        
        
        
      
    }
    
    func setCalendarViewLayout(){
        
        
        let height:CGFloat?
        
        if UIScreen.main.nativeBounds.height <= 1136{
            print("SE SE SE")
            height = self.mainCalendarView.frame.height * 0.5
            
        }else{
            
            height = self.mainCalendarView.frame.height * 0.7
        }
        
        if calendarView.frame == .zero {
            calendarView.frame = CGRect(
                x: 0,
                y: weekdayView.frame.maxY,
                width: self.mainCalendarView.frame.width,
                height: height ?? self.mainCalendarView.frame.height * 0.7
            )
            calendarView.setup()
        }
        
        print("calendarView.frame",calendarView.frame)
        print("mainCalendarView.frame",mainCalendarView.frame)
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//
    
        //setup Celendar
        
        
   //     setCelendar()
        
        
        
//        print("viewDidLayoutSubviews")
        
//        let height:CGFloat?
//
//        if UIScreen.main.nativeBounds.height <= 1136{
//            print("SE SE SE")
//            height = self.mainCalendarView.frame.height * 0.5
//
//        }else{
//
//            height = self.mainCalendarView.frame.height * 0.7
//        }
//
//        if calendarView.frame == .zero {
//            calendarView.frame = CGRect(
//                x: 0,
//                y: weekdayView.frame.maxY,
//                width: self.mainCalendarView.frame.width,
//                height: height ?? self.mainCalendarView.frame.height * 0.7
//            )
//            calendarView.setup()
//        }
        
        
        
        
      //  print("calendarView.frame",calendarView.frame)
//        print("mainCalendarView.frame",mainCalendarView.frame)
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
        if isForOrigin{
            
          return places.count
            
        }else if (isForDestination){
            
            return destinationPlaces.count
        }else
        
        {return 0}
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "placeCell")!
        
            let place = cell.viewWithTag(1) as! UILabel
        if isForOrigin{
            
            place.text = places[indexPath.row].localized
            
        }else{
            
            place.text = destinationPlaces[indexPath.row].localized
        }
        
        if Language.language == Language.arabic{
            
            place.font = UIFont(name:"Cairo-Bold",size:17)
        }
            return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("isForOrigin isForDestination",isForOrigin,isForDestination)
        
        if isForOrigin{
            
            selectedPlace = places[indexPath.row]
            
        }else{
            
            selectedPlace = destinationPlaces[indexPath.row]
            
        }
        
        isSelectPlace = true
        hidePopUpView()
        
    }
    
    @IBAction func returnButtonAction(_ sender: UIButton) {
        
        if  oneWayButton.isSelected {
            
            oneWayButton.isSelected = false
            sender.isSelected = true
            
            setFirstView()
            
            topLabel.text = "Select Travel Dates".localized
            
            calendarView.removeFromSuperview()
            setCelendar()
            
            //popupView.isHidden = true
            
            returnLabel.font = returnLabel.font.bold()
            oneWayLabel.font = oneWayLabel.font.regular()
        }
        
    }
    
    
    //passenger view methods
    
    @IBAction func adultPlusButtonAction(_ sender: Any) {
        
        if  adultPassenger + childPassenger < 9 {
            
            adultPassenger += 1
            
            adultCountLabel.text = "\(adultPassenger)"
            
            if adultPassenger > 1{
                
                adultLabel.text = "Adults".localized
            }
        }
        
        
        
    }
    
    @IBAction func adultMinusButtonAction(_ sender: Any) {
        
        adultPassenger -= 1
        
        if adultPassenger <= 1 {
            
            adultPassenger = 1
            adultLabel.text = "Adult".localized

        }
        
        adultCountLabel.text = "\(adultPassenger)"
    }
    
    @IBAction func childPlusButtonAction(_ sender: Any) {
        
        if adultPassenger + childPassenger < 9{
            
            childPassenger += 1
            
            if childPassenger > 1{
                
                childLabel.text = "Children".localized
            }
            
            childCountLabel.text = "\(childPassenger)"
        }
        
    }
    
    @IBAction func childMinusButtonAction(_ sender: Any) {
        
        childPassenger -= 1
        
        if childPassenger < 0 {
            
            childPassenger = 0
        }
        
        if childPassenger <= 1 {
            
            childLabel.text = "Child".localized
            
        }
        
        childCountLabel.text = "\(childPassenger)"
    }
    
    @IBAction func infantPlusButtonAction(_ sender: Any) {
        
        
            infantPassenger += 1
        
            if infantPassenger > adultPassenger{
            
               infantPassenger = adultPassenger
            
            
            
            }
            if infantPassenger > 1{
            
               infantLabel.text = "Infants".localized
            }
        
            
            infantCountLabel.text = "\(infantPassenger)"
        
    }
    
    @IBAction func infantMinusButtonAction(_ sender: Any) {
        
        infantPassenger -= 1
        

        
        if infantPassenger < 0 {
            
            infantPassenger = 0
        }
        
        if infantPassenger <= 1{
            
            infantLabel.text = "Infant".localized
        }
        infantCountLabel.text = "\(infantPassenger)"
    }
    
    
    
    
    
    
    
    @IBAction func oneWayButtonAction(_ sender: UIButton) {
        
        if  returnButton.isSelected {
            
            returnButton.isSelected = false
            sender.isSelected = true
            
            oneWaydatestring = firstselectedDay.toDateString(format: "yyyy-MM-dd")
            
            setFirstView()
            
            topLabel.text = "Select Travel Date".localized
            
            calendarView.removeFromSuperview()
            setCelendar()
            
            //popupView.isHidden = true
            
            oneWayLabel.font = oneWayLabel.font.bold()
            returnLabel.font = returnLabel.font.regular()
        }
    }
    
    
    
    @IBAction func languageButtonAction(_ sender: Any) {
        
        if Language.language == Language.english{
            
            
            Language.language = Language.arabic
            print("??????????",Language.language)
            
        }else{
            
            Language.language = Language.english
            
            
        }
    }
    
    @IBAction func dateOKButtonAction(_ sender: Any) {
        
        print("calendar?.selectedDays",calendar?.selectedDays.count ?? 0)
        
        if returnButton.isSelected{
            
            if calendar?.selectedDays.count != 2{
                
                print("2 date not selected")
                let alertVC = UIAlertController(title: nil, message: "Please select two dates.".localized, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                    self.dismiss(animated: true, completion: nil)
                    
                })
                
                alertVC.addAction(okAction)
                self.present(alertVC, animated: true, completion: nil)
                
            }else{
                
                hidePopUpView()
            }
            
        }else{
            
               hidePopUpView()
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        promoTextView.resignFirstResponder()
        
    }
    
    @IBAction func promoOKButtonAction(_ sender: Any) {
        
        if !((promoTextView.text?.isEmpty)!){
            
            isPromoAdded = true
            self.promoCode = promoTextView.text!
            
            hidePopUpView()
            
            
            
        }else{
            
            let alertVC = UIAlertController(title: nil, message: "Please enter your promo code.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alertVC.addAction(okAction)
            self.present(alertVC, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    @IBAction func cancelButtonAaction(_ sender: Any) {
        
        
        
        hidePopUpView()
        
    }
    
    
    @IBAction func addPromoButtonAction(_ sender: Any) {
        
        popPlaceView.isHidden = true
        popDateView.isHidden = true
        popPassengerView.isHidden = true
        popPromoView.isHidden = false
        
        showPopUpView()
        self.promoTextView.becomeFirstResponder()
    }
    
    
    @IBAction func searchFlightButtonAction(_ sender: Any) {
        
        //https://book.flyadeal.com/#/booking/search-redirect?culture=en-GB&flightType=round&originCode=MED&destinationCode=RUH&adults=1&children=0&infants=0&departureDate=2018-12-25&arriveDate=2018-12-27
        
        var urlString = "https://book.flyadeal.com"
        
        if returnButton.isSelected {
            
            if isPromoAdded{
                
                //https://book.flyadeal.com/#/booking/search-redirect?culture=en-GB&flightType=oneway&originCode=JED&destinationCode=ERUH&adults=1&children=0&infants=0&departureDate=2018-12-09&arriveDate=null&pc=ABC
                
               urlString = "https://book.flyadeal.com/#/booking/search-redirect?culture=\(culture)&flightType=round&originCode=\(fromPlaceShortLabel.text!)&destinationCode=\(toPlaceShortLabel.text!)&adults=\(adultPassenger)&children=\(childPassenger)&infants=\(infantPassenger)&departureDate=\(firstdatestring!)&arriveDate=\(lastdatestring!)&pc=\(promoCode))"
            }else{
                
                urlString = "https://book.flyadeal.com/#/booking/search-redirect?culture=\(culture)&flightType=round&originCode=\(fromPlaceShortLabel.text!)&destinationCode=\(toPlaceShortLabel.text!)&adults=\(adultPassenger)&children=\(childPassenger)&infants=\(infantPassenger)&departureDate=\(firstdatestring!)&arriveDate=\(lastdatestring!)"
            }
            
        }else{
            
            if isPromoAdded{
                
                urlString = "https://book.flyadeal.com/#/booking/search-redirect?culture=\(culture)&flightType=oneway&originCode=\(fromPlaceShortLabel.text!)&destinationCode=\(toPlaceShortLabel.text!)&adults=\(adultPassenger)&children=\(childPassenger)&infants=\(infantPassenger)&departureDate=\(oneWaydatestring!)&arriveDate=null&pc=\(promoCode)"
                
            }else{
                
                urlString = "https://book.flyadeal.com/#/booking/search-redirect?culture=\(culture)&flightType=oneway&originCode=\(fromPlaceShortLabel.text!)&destinationCode=\(toPlaceShortLabel.text!)&adults=\(adultPassenger)&children=\(childPassenger)&infants=\(infantPassenger)&departureDate=\(oneWaydatestring!)&arriveDate=null"
            }
           
        }
        
        
       
        if let url = URL(string: urlString) {

            print("urlString",url)

        //    UIApplication.shared.open(url, options: [:])
            
            let svc = SFSafariViewController(url: url)

                self.present(svc, animated: true) {

                  print("open safari")
            }

        }
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//           vc.urlString = urlString
//        self.navigationController?.pushViewController(vc, animated: true)

        
    }
    
    
    
    
    func showPopUpView(){
        
//        topLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        selectOriginLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        passengerTopLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        promoTopLabel.roundCorners([.topLeft,.topRight], radius: 5)
//

        setCalendarViewLayout()
        
        print("calendarView.frame in showPopUpView",calendarView.frame)
        print("mainCalendarView.frame in showPopUpView",mainCalendarView.frame)
       
        popupView.isHidden = false
        
        popupView.frame = CGRect(x:0, y:self.view.frame.height, width:self.popupView.frame.width, height:self.popupView.frame.size.height)
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.popupView.frame = CGRect(x:0, y:0, width:self.popupView.frame.width, height:self.popupView.frame.size.height)
            
            
        }) { (true) in
            
            print("animate done")
            
        }
    }
    
    func hidePopUpView(){
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.popupView.frame = CGRect(x:0, y:self.view.frame.height, width:self.popupView.frame.width, height:self.popupView.frame.size.height)
            
            
        }) { (true) in
            
            //to set passenger label
            self.setPassenger()
            
            if self.isForOrigin{
                
                if self.isSelectPlace{
                    
                    self.fromPlaceNameLabel.text = self.selectedPlace.localized
                    self.isForOrigin = false
                    self.fromPlaceShortLabel.text = self.getShortFormForPlace(forString: self.fromPlaceNameLabel.text!)
                    
                    self.isSelectPlace = false
                }
                
            }else if self.isForDestination{
                
                print("self.selectedPlace",self.selectedPlace)
                if self.isSelectPlace{
                    
                    self.toPlacenameLabel.text = self.selectedPlace.localized
                    self.isForDestination = false
                    self.toPlaceShortLabel.text = self.getShortFormForPlace(forString: self.toPlacenameLabel.text!)
                    
                    self.isSelectPlace = false
                }
                
            }
            
            
            
            print("animate hide")
            self.popupView.isHidden = true
            
            self.totalPassenger = self.adultPassenger + self.childPassenger + self.infantPassenger
            
            print("self.totalPassenger",self.totalPassenger)
            
            self.promoTextView.resignFirstResponder()
        }

    }
    
    
    @IBAction func homeButtonAction(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func getShortFormForPlace(forString:String) -> String{
        
        print("forString",forString)
        
        if forString == "Dammam" || forString == "الدمام"{
            
            return "DMM"
            
            
        }else if (forString == "Jeddah") || forString == "جدة"{
            
            return "JED"
            
        }else if (forString == "Abha") || forString == "أبها" {
            
            return "AHB"
            
        }else if (forString == "Qassim") || forString == "القصيم"{
            
            return "ELQ"
            
        }else if (forString == "Jazan") || forString == "جازان"{
            
            return "GIZ"
            
        }else if (forString == "Madinah") || forString == "المدينة المنورة"{
            
            return "MED"
            
        }else if (forString == "Riyadh") || forString == "الرياض"{
            
            return "RUH"
        }else if (forString == "Tabuk") || forString == "تبوك"{
            
            return "TUU"
            
        }else{
            
            return ""
        }
    
    }
    
    

}



extension BookFlightViewController: VADayViewAppearanceDelegate{
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 214 / 255, green: 214 / 255, blue: 219 / 255, alpha: 1.0)
        case .selected:
            return .white
        case .unavailable:
            return .lightGray
        default:
            return .black
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return UIColor(red: 59 / 255, green: 36 / 255, blue: 83 / 255, alpha: 1.0)
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .square
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -7
        }
    }
    
}
extension BookFlightViewController: VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView.nextMonth()
    }
    
    func didTapPreviousMonth() {
        calendarView.previousMonth()
    }
    
}


extension BookFlightViewController: VACalendarViewDelegate {
    
    func selectedDates(_ dates: [Date]) {
        
        calendarView.startDate = dates.last ?? Date()
        print("dates ",dates,dates.count)
        let selectedVday = VADay(date: calendarView.startDate , state: .selected, calendar: Calendar.current)
        
        if  dates.count > 2 {
            
            calendar?.deselectAll()
            
            calendar?.setDaySelectionState(selectedVday, state: .selected)
            calendarView.calendarDelegate?.selectedDate?(selectedVday.date)

            
        }
        
        if dates.count == 2 {
            
            
            if dates.first! < dates.last!{
                
                
                firstdatestring =  dates.first!.toDateString(format: "yyyy-MM-dd")
                print("firstDate......???????",firstdatestring!)
                
                lastdatestring =  dates.last!.toDateString(format: "yyyy-MM-dd")
                print("lastDate......???????",lastdatestring!)
                
                
                firstReadabledatestring = dates.first!.toDateString(format: "d MMM, yyyy").localized
                lastReadabledatestring = dates.last!.toDateString(format: "d MMM, yyyy").localized
               
                
                dateLabel.text = "\(firstReadabledatestring!) - \(lastReadabledatestring!)"
                
            }else{
                
                firstdatestring =  dates.last!.toDateString(format: "yyyy-MM-dd")
                print("firstDate....last..???????",firstdatestring!)
                
                lastdatestring =  dates.first!.toDateString(format: "yyyy-MM-dd")
                print("lastDate......???????",lastdatestring!)
                
                firstReadabledatestring = dates.last!.toDateString(format: "d MMM, yyyy").localized
                lastReadabledatestring = dates.first!.toDateString(format: "d MMM, yyyy").localized
               
                
                dateLabel.text = "\(firstReadabledatestring!) - \(lastReadabledatestring!)"
                
            }
            
        }


}
        
    func selectedDate(_ date: Date) {
        
        //calendarView.startDate =  Date()

        print(date)
        
       let datestring =  date.toDateString(format: "yyyy-MM-dd")
        
        if oneWayButton.isSelected {
            
            oneWaydatestring = datestring
            
            print("oneWaydatestring.....in selectedday.",oneWaydatestring!)
            
            
            oneWayReadabledatestring = date.toDateString(format: "d MMM, yyyy").localized
            
            
            dateLabel.text = "\(oneWayReadabledatestring!)"
        }
        
        
    }
    
}

extension UIFont {
    
    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor
            .withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }
    

    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
    func regular() -> UIFont {
        return withTraits(traits: .traitLooseLeading)
    }

}



