//
//  ManageViewController.swift
//  Flyadeal
//
//  Created by Md.Ballal Hossen on 9/12/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import SafariServices

class ManageViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var bookingRefView: UIView!
    
    @IBOutlet weak var lastNameView: UIView!
    
    @IBOutlet weak var popUpView: UIView!
    
    @IBOutlet weak var bookRefPopView: UIView!
    
    @IBOutlet weak var lastNamePopView: UIView!
    
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var lastNameTopLabel: UILabel!
    
    
    @IBOutlet weak var referanceTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var manageBookingLabel: UILabel!
    
    
    @IBOutlet weak var bookingReferenceLabel: UILabel!
    
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var welComeEnLabel: UILabel!
    @IBOutlet weak var welcomeArLabel: UILabel!
    
    @IBOutlet weak var logoFroAr: UIImageView!
    
    @IBOutlet weak var logoForEn: UIImageView!
    
    @IBOutlet weak var lastNLabel: UILabel!
    
    @IBOutlet weak var lastNameLabel: UILabel!
    
    
    @IBOutlet weak var okButtonInRefView: UIButton!
    @IBOutlet weak var cancelButtonInREfView: UIButton!
    
    @IBOutlet weak var okButtonInLastNView: UIButton!
    @IBOutlet weak var cancelButtonInLastNView: UIButton!
    
    
    var isEditingBookingRef:Bool = false
    var isEditingLastName:Bool = false
    
    var culture:String = "en-GB"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.welComeEnLabel.text = "Welcome".localized
         self.welcomeArLabel.text = "Welcome".localized
        
         manageBookingLabel.text = "Manage booking".localized
         bookingReferenceLabel.text = "Booking Reference".localized
         lastNLabel.text = "Last Name".localized

        continueButton.setTitle("Continue".localized, for: .normal)
        topLabel.text = "Booking Reference".localized
        lastNameTopLabel.text = "Last Name".localized

        
        okButtonInRefView.setTitle("OK".localized, for: .normal)
        cancelButtonInREfView.setTitle("Cancel".localized, for: .normal)
        okButtonInLastNView.setTitle("OK".localized, for: .normal)
        cancelButtonInLastNView.setTitle("Cancel".localized, for: .normal)
        

        

        
//        topLabel.roundCorners([.topLeft,.topRight], radius: 5)
//        lastNameTopLabel.roundCorners([.topLeft,.topRight], radius: 5)

        
        let tapOnReferenceView = UITapGestureRecognizer(target: self, action: #selector(tappedInFeferenceView(tapGestureRecognizer:)))
        
        bookingRefView.addGestureRecognizer(tapOnReferenceView)
        
        let tapOnNameView = UITapGestureRecognizer(target: self, action: #selector(tappedInNameView(tapGestureRecognizer:)))
        
        lastNameView.addGestureRecognizer(tapOnNameView)

        
        referanceTextField.delegate = self
        lastNameTextField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Language.language == Language.english{
            
            print("??????????",Language.language)
            
            culture = "en-GB"
            
            welComeEnLabel.isHidden = false
            welcomeArLabel.isHidden = true
            
            logoForEn.isHidden = false
            logoFroAr.isHidden = true
            

            
        }else{
            
            welComeEnLabel.isHidden = true
            welcomeArLabel.isHidden = false
            
            logoForEn.isHidden = true
            logoFroAr.isHidden = false
            

           culture = "ar-SA"
            
            
            manageBookingLabel.font = UIFont(name:"Cairo-Bold",size:20)
            
            bookingReferenceLabel.font = UIFont(name:"Cairo",size:13)
            lastNLabel.font = UIFont(name:"Cairo",size:13)
            referenceLabel.font = UIFont(name:"Cairo",size:18)
            lastNameLabel.font = UIFont(name:"Cairo",size:18)
            
            topLabel.font = UIFont(name:"Cairo-Bold",size:18)
            
            lastNameTopLabel.font = UIFont(name:"Cairo-Bold",size:18)
            
            
           
            
            continueButton.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            okButtonInRefView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            okButtonInLastNView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            cancelButtonInREfView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            
            cancelButtonInLastNView.titleLabel?.font = UIFont(name:"Cairo-Bold",size:18)
            

            
        }
        
    }
    
    @objc func tappedInFeferenceView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("tap .............tappedInFeferenceView..")
        
        bookRefPopView.isHidden = false
        lastNamePopView.isHidden = true
        
        showPopUpView()
        
        self.referanceTextField.becomeFirstResponder()
    }
    
    @objc func tappedInNameView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("tap .............tappedInNameView..")
        
        bookRefPopView.isHidden = true
        lastNamePopView.isHidden = false
        
        showPopUpView()
        
        self.lastNameTextField.becomeFirstResponder()
    }
    
    // To hide keyboard when touch on the view (outside the UITextField)
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        referanceTextField.resignFirstResponder()
        lastNameTextField.resignFirstResponder()
    }
    

    func showPopUpView(){
        
        popUpView.isHidden = false
        
        popUpView.frame = CGRect(x:0, y:self.view.frame.height, width:self.popUpView.frame.width, height:self.popUpView.frame.size.height)
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.popUpView.frame = CGRect(x:0, y:0, width:self.popUpView.frame.width, height:self.popUpView.frame.size.height)
            
            
        }) { (true) in
            
//            if !self.bookingRefView.isHidden{
//
//               self.referanceTextField.becomeFirstResponder()
//
//            }else if !self.lastNamePopView.isHidden{
//
//               self.lastNameTextField.becomeFirstResponder()
//            }
            
            print("animate done")
            
        }
    }
    
    func hidePopUpView(){
        
//        self.popUpView.frame = CGRect(x:0, y:0, width:self.popUpView.frame.width, height:self.popUpView.frame.size.height)

        UIView.animate(withDuration: 0.3, animations: {
            
            self.popUpView.frame = CGRect(x:0, y:self.view.frame.height, width:self.popUpView.frame.width, height:self.popUpView.frame.size.height)
            
            
        }) { (true) in
            
            print("animate hide")
            self.referanceTextField.resignFirstResponder()
            self.lastNameTextField.resignFirstResponder()
            
            self.popUpView.isHidden = true
            
            if self.isEditingBookingRef{
                
                self.referenceLabel.text = self.referanceTextField.text
                self.isEditingBookingRef = false
                self.referanceTextField.resignFirstResponder()
                
            }
            if self.isEditingLastName{
                
                self.lastNameLabel.text = self.lastNameTextField.text
                self.isEditingLastName = false
                self.lastNameTextField.resignFirstResponder()
                
            }
            
        }
        
    }
    @IBAction func bRefOKButtonAction(_ sender: Any) {
        
        if !((referanceTextField.text?.isEmpty)!){
            
            if !((referanceTextField.text?.count)! == 6){
                
                print("referenceLabel.text?.count",(referanceTextField.text?.count)!)
                
                let alertVC = UIAlertController(title: nil, message: "Please ensure that Booking Reference is 6 characters.".localized, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                    self.dismiss(animated: true, completion: nil)
                    
                })
                
                alertVC.addAction(okAction)
                self.present(alertVC, animated: true, completion: nil)
                
            }else{

                isEditingBookingRef = true
            
                hidePopUpView()
            }
            
           
        }else{
            
            let alertVC = UIAlertController(title: nil, message: "Please enter booking reference.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alertVC.addAction(okAction)
            self.present(alertVC, animated: true, completion: nil)
            
        }

    }
    
    @IBAction func LnameOKButtonAction(_ sender: Any) {
        
        if !((lastNameTextField.text?.isEmpty)!){
            
            isEditingLastName = true
            
            hidePopUpView()
            
           
            
        }else{
            
            let alertVC = UIAlertController(title: nil, message: "Please enter your last name.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alertVC.addAction(okAction)
            self.present(alertVC, animated: true, completion: nil)
            
        }
        
    }
    

    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        hidePopUpView()
    }
    
    
    @IBAction func languageChangeButtonAction(_ sender: Any) {
        
        if Language.language == Language.english{
            
            
            Language.language = Language.arabic
            print("??????????",Language.language)
            
        }else{
            
            Language.language = Language.english
            
            
        }
    }
    
    
    @IBAction func continueButtonAction(_ sender: Any) {
        
        if !((referenceLabel.text?.isEmpty)!) &&  !((lastNameLabel.text?.isEmpty)!){
            
            print("referenceLabel.text?.count",(referenceLabel.text?.count)!)
            
            
                
                let urlString = "https://book.flyadeal.com/#/manage/search-redirect?culture=\(culture)&pnr=\(referenceLabel.text!)&lastName=\(lastNameLabel.text!)"
                
                print("urlString",urlString)
                
                if let url = URL(string: urlString) {
                    
                   // UIApplication.shared.open(url, options: [:])
                    let svc = SFSafariViewController(url: url)
                    
                    self.present(svc, animated: true) {
                        
                        print("open safari")
                    }

                
                }
            
            
        }else{
            
            let alertVC = UIAlertController(title: nil, message: "Please give your input correctly.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (aciton) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alertVC.addAction(okAction)
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func homeButtonAction(_ sender: Any) {
   self.navigationController?.popToRootViewController(animated: true)
    }
    
}
